package com.ctrlappz.otlob.utils


object Links {

    var URL = "http://5.5.5.39:8000/"
    var API = "api/v1"
    var LOGIN = "/login"
    var SIGN_UP = "/register"
    var USER = "/user"
    var WORKER = "/worker"
    var SERVICES = "/services"
    var WORKERS = "/workers"
}